﻿using System;

namespace ListManipulation
{
	public interface INamable
	{
		string Name {
			get;
			set;
		}
	}
}


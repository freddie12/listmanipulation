using Cirrious.MvvmCross.ViewModels;
using System.Collections.Generic;
using ListManipulation.Core.Data;

namespace ListManipulation.ViewModels
{
    public class MainViewModel : MvxViewModel
    {
		private string[] strings = {
			"0",
			"1",
			"2",
			"3",
			"4",
			"5",
			"6",
			"7",
			"8",
		};

		public MainViewModel ()
		{
			InitData ();	
		}

		private void InitData ()
		{
			Names = new List <List<ListItem>> ();
			foreach (string a in strings) {
				var tempList = new List <ListItem> ();
				foreach (string b  in strings) {
					tempList.Add (new ListItem () { Name = a + b });
				}
				Names.Add (tempList);
			}

		}

		private List <List<ListItem>> names;
		public List <List<ListItem>> Names
		{
			get { return names; }
			set { names = value; }
		}
    }
}

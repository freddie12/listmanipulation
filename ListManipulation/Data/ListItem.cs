﻿using System;

namespace ListManipulation.Core.Data
{
	public class ListItem : INamable
	{
		public string Name {
			get;
			set;
		}
	}
}


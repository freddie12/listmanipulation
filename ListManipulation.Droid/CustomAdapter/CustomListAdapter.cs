﻿using System;
using Android.Widget;
using Android.Content;
using Android.Views;
using ListManipulation.Droid.CustomView.List;
using System.Collections.Generic;
using ListManipulation.Core.Data;

namespace ListManipulation.Droid
{
	public class MyAdapter : BaseAdapter
	{
		private Context mContext;
		private ViewHolder viewHolder;

		public MyAdapter (Context context) 
		{
			mContext = context;
		}

		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			if (convertView == null) {
				convertView = LayoutInflater.From (mContext).Inflate (Resource.Layout.horizontal_list_layout, parent, false);
				viewHolder = new ViewHolder ();
				convertView.Tag = viewHolder;
			} else {
				viewHolder = (ViewHolder) convertView.Tag;
			}

			((HorizontalListView)convertView).INDEX = position;
			((HorizontalListView)convertView).Adapter.ItemsSource = itemsSource [position];

			return convertView;
		}

		public override int Count {
			get {
				return itemsSource.Count;
			}
		}

		public class ViewHolder : Java.Lang.Object {}

		private List<List<ListItem>> itemsSource = new List<List<ListItem>> ();
		public List<List<ListItem>> ItemsSource {
			get { return itemsSource; }
			set { itemsSource = value; }
		}

		public override long GetItemId (int position)
		{
			return 1;
		}

		public override Java.Lang.Object GetItem (int position)
		{
			return (Java.Lang.Object) "";
		}
	}
}


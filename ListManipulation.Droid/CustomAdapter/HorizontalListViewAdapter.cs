﻿using System;
using Android.Widget;
using System.Collections.Generic;
using Android.Content;
using Android.Views;

namespace ListManipulation.Droid.CustomAdapter
{
	public class HorizontalListViewAdapter <T> : BaseAdapter where T : INamable 
	{
		
		private Context mContext;
		private ViewHolder mViewHolder;

		public HorizontalListViewAdapter (Context context) 
		{
			mContext = context;
		}

		public HorizontalListViewAdapter (Context context, List <T> items) : this (context)
		{
			mItemsSource = items;
		}

		private List <T> mItemsSource = new List <T> ();
		public List <T> ItemsSource 
		{
			get { return mItemsSource; }
			set { mItemsSource = value; }
		}

		public override int Count {
			get { return mItemsSource.Count; }
		}

		public override long GetItemId (int position)
		{
			return 1;
		}

		public override Java.Lang.Object GetItem (int position)
		{
			return (Java.Lang.Object) "";
		}

		public override View GetView (int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = LayoutInflater.From (mContext).Inflate (Resource.Layout.list_item, parent, false);
				mViewHolder = new ViewHolder ();
				mViewHolder.Name = convertView.FindViewById <TextView> (Resource.Id.text_view1);
				convertView.Tag = mViewHolder;
			} else {
				mViewHolder = (ViewHolder) convertView.Tag;
			}

			mViewHolder.Name.Text = ((INamable) ItemsSource [position]).Name;

			return convertView;
		}

		public class ViewHolder : Java.Lang.Object
		{
			public TextView Name
			{
				get;
				set;
			}
		}
	}
}


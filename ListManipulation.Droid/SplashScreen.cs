using Android.App;
using Android.Content.PM;
using Cirrious.MvvmCross.Droid.Views;

namespace ListManipulation.Droid
{
    [Activity(
		Label = "ListManipulation.Droid"
		, MainLauncher = true
		, Theme = "@style/Theme.Splash"
		, NoHistory = true
		, ScreenOrientation = ScreenOrientation.Portrait)]
    public class SplashScreen : MvxSplashScreenActivity
    {
        public SplashScreen() : base(Resource.Layout.SplashScreen)
        {
        }
    }
}
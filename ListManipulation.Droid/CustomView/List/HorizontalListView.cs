﻿using System;
using Android.Widget;
using Android.Content;
using Android.Util;
using Android.Views;
using System.Collections.Generic;
using ListManipulation.Droid.CustomAdapter;
using ListManipulation.Core.Data;

namespace ListManipulation.Droid.CustomView.List
{
	public class HorizontalListView : AdapterView
	{
		private HorizontalListViewAdapter <ListItem> mAdapter;
		int mTouchStartX = 0;
		int mTouchStartY = 0;
		int mListLeftStart = 0;
		bool touchInteceptRequested;
		int touchSlop;

		int mNewLeftStart = 0;
		int mLeftViewIndex = 0;
		int mRightViewIndex = 0;
		int mListLeftMinusRemovedViews = 0;
		const int LAYOUT_MODE_RIGHT = -1;
		const int LAYOUT_MODE_LEFT = 0;

		private Queue <View> mCachedItemViews {
			get;
			set;
		} = new Queue <View> ();

		public HorizontalListView (Context context, IAttributeSet attrs) : base (context, attrs) 
		{
			touchSlop = ViewConfiguration.Get (context).ScaledTouchSlop;
			mAdapter = new HorizontalListViewAdapter<ListItem> (context);
		}

		public int INDEX {
			set;
			get;
		}

		protected override Java.Lang.Object RawAdapter {
			get 
			{
				return Adapter;
			}
			set
			{
				Adapter = (HorizontalListViewAdapter <ListItem>) value;
			}
		}

		public HorizontalListViewAdapter <ListItem> Adapter {
			get { 
				return mAdapter; 
			}
			set { 
				mAdapter = value; 
				RemoveAllViewsInLayout ();
				RequestLayout ();
			}
		}

		public override View SelectedView {
			get;
		}

		public override void SetSelection (int position)
		{
			throw new Exception ("Set Selection Exception");
		}

		protected override void OnLayout (bool changed, int left, int top, int right, int bottom)
		{
			base.OnLayout (changed, left, top, right, bottom);

			// if we don't have an adapter, we don't need to do anything
			if (mAdapter == null)
				return;

			if (ChildCount == 0) {
				mRightViewIndex = -1;
				FillListRight (mNewLeftStart, 0);
			} else {
				int offset = mNewLeftStart + mListLeftMinusRemovedViews - GetChildAt(0).Left;
				RemoveNonVisibleViews (offset);
				FillList (offset);
			}

			PositionItems ();
		}

		protected override void OnMeasure (int widthMeasureSpec, int heightMeasureSpec)
		{
			// This is where the height is always zero unless I know the child height. 
			// If I don't override this then the base will not wrap around the children height automatically
			var child = GetChildAt (0);
			if (child == null) {
				base.OnMeasure (widthMeasureSpec, heightMeasureSpec);
				return;
			}
			
			var width = View.ResolveSize (Width, widthMeasureSpec);

			var height = View.ResolveSize (child.MeasuredHeight, heightMeasureSpec);

			SetMeasuredDimension (width, height);
		}
			
		private void RemoveNonVisibleViews (int offset)
		{
			// We need to keep close track of the child count in this function. We
			// should never remove all the views, because if we do, we loose track
			// of were we are.
			int childCount = ChildCount;

			// if we are not at the right of the list and have more than one child
			if (mRightViewIndex != mAdapter.Count - 1 && childCount > 1) {
				// check if we should remove any views in the top
				var firstChild = GetChildAt (0);
				while (firstChild != null && firstChild.Right + offset < 0) {
					// remove the top view
					RemoveViewInLayout (firstChild);
					childCount--;
					mCachedItemViews.Enqueue (firstChild);
					mLeftViewIndex++;

					// update the list offset (since we've removed the top child)
					mListLeftMinusRemovedViews += firstChild.MeasuredWidth;

					// Continue to check the next child only if we have more than
					// one child left
					if (childCount > 1) {
						firstChild = GetChildAt (0);
					} else {
						firstChild = null;
					}
				}
			}

			// if we are not at the left of the list and have more than one child
			if (mLeftViewIndex != 0 && childCount > 1) {
				// check if we should remove any views in the bottom
				View lastChild = GetChildAt (childCount - 1);
				while (lastChild != null && lastChild.Left + offset > Width) {
					// remove the bottom view
					RemoveViewInLayout (lastChild);
					childCount--;
					mCachedItemViews.Enqueue (lastChild);
					mRightViewIndex--;

					// Continue to check the next child only if we have more than
					// one child left
					if (childCount > 1) {
						lastChild = GetChildAt (childCount - 1);
					} else {
						lastChild = null;
					}
				}
			}
		}

		private void FillList (int offset)
		{
			int rightEdge = GetChildAt (ChildCount - 1).Right;
			FillListRight (rightEdge, offset);

			int leftEdge = GetChildAt (0).Left;
			FillListLeft (leftEdge, offset);
		}

		private void FillListLeft (int leftEdge, int offset) {
			while (leftEdge + offset > 0 && mLeftViewIndex > 0) {
				mLeftViewIndex--;
				var newTopCild = mAdapter.GetView (mLeftViewIndex, GetCachedView(), this);
				AddAndMeasureChild (newTopCild, LAYOUT_MODE_LEFT);
				int childWidth = newTopCild.MeasuredWidth;
				leftEdge -= childWidth;

				// update the list offset (since we added a view at the top)
				mListLeftMinusRemovedViews -= childWidth;
			}
		}

		private void FillListRight (int rightEdge, int offset)
		{
			while (rightEdge + offset < Width && mRightViewIndex < mAdapter.Count - 1) {
				mRightViewIndex++;
				var newBottomchild = mAdapter.GetView (mRightViewIndex, GetCachedView(), this);
				AddAndMeasureChild (newBottomchild, LAYOUT_MODE_RIGHT);
				rightEdge += newBottomchild.MeasuredWidth;
			}
		}

		private void AddAndMeasureChild (View child, int layoutMode)
		{
			var _params = child.LayoutParameters;
			if (_params == null) {
				_params = new LayoutParams(-2, -2);
			}

			AddViewInLayout (child, layoutMode, _params, true);

			int itemWidth = Width;
			child.Measure (MeasureSpec.MakeMeasureSpec(0, MeasureSpecMode.Unspecified), MeasureSpec.MakeMeasureSpec(0, MeasureSpecMode.Unspecified));
		}

		private View GetCachedView()
		{
			if (mCachedItemViews.Count != 0) {
				return mCachedItemViews.Dequeue ();
			}
			return null;
		}



		private void PositionItems ()
		{
			var top = 0;
			var left = mNewLeftStart + mListLeftMinusRemovedViews;
			for (int index = 0; index < ChildCount; index++) 
			{
				var child = GetChildAt (index);
				int childWidth = child.MeasuredWidth;
				int childHeight = child.MeasuredHeight;
				child.Layout (left, top, left + childWidth, top + childHeight);
				left += childWidth;
			}
		}

		public override bool OnTouchEvent (MotionEvent e)
		{
			if (ChildCount == 0)
				return false;

			switch (e.Action) {
			case MotionEventActions.Down:
				mTouchStartX = (int)e.GetX ();
				mTouchStartY = (int)e.GetY ();
				mListLeftStart = (int)GetChildAt (0).Left;
				break;

			case MotionEventActions.Move:
				int scrolledDistance = (int)e.GetX () - mTouchStartX;

				if (!touchInteceptRequested) {
					if (HorizontalScroll (mTouchStartX, (int)e.GetX (), mTouchStartY, (int)e.GetY ())) {
						Parent.RequestDisallowInterceptTouchEvent (true);
						touchInteceptRequested = true;
					} else {
						return false;
					}
				} 
				if (scrolledDistance > touchSlop || scrolledDistance * -1 > -touchSlop) {
					mNewLeftStart = mListLeftStart + scrolledDistance;
					RequestLayout ();
				}
				break;
			case MotionEventActions.Up:
				touchInteceptRequested = false;
				mListLeftMinusRemovedViews = 0;
				break;

			}
			return true;
		}

		private bool HorizontalScroll (int mTouchStartX, int x, int mTouchStartY, int y)
		{
			var vDiff = mTouchStartY - y;
			var hDiff = mTouchStartX - x;

			if (vDiff < 0)
				vDiff *= -1;

			if (hDiff < 0)
				hDiff *= -1;

			return hDiff > vDiff;
		}
	}


}


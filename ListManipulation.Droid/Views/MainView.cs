using Android.App;
using Android.OS;
using Cirrious.MvvmCross.Droid.Views;
using Android.Widget;
using ListManipulation.ViewModels;
using ListManipulation.Droid.CustomView.List;
using Android.Content;
using Android.Views;
using System.Collections.Generic;
using ListManipulation.Core.Data;

namespace ListManipulation.Droid.Views
{
    [Activity(Label = "MainView")]
    public class MainView : MvxActivity
    {
		private ListView mListView;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
			SetContentView(Resource.Layout.MainView);

			GetViewReferences ();
			InitialiseList ();
        }

		private void InitialiseList ()
		{
			mListView.Adapter = new MyAdapter ((Context) this);
			((MyAdapter)mListView.Adapter).ItemsSource = ((MainViewModel) ViewModel).Names;
		}



		private void GetViewReferences ()
		{
			mListView = FindViewById <ListView> (Resource.Id.list_view1);
		}
    }
}